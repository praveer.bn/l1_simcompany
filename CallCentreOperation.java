package simcompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Formatter;
import java.util.Random;
import java.util.Scanner;

public class CallCentreOperation {
    Connection connection;
    PreparedStatement preparedStatement;
    Statement statement;
    ResultSet resultSet;
BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
public void prepaid() throws IOException {
    System.out.println("enter the moblie number of user ");
    Scanner scanner = new Scanner(System.in);
    long mobilenumber = scanner.nextLong();
    System.out.println("enter 0 for check balnce and validity ");
    System.out.println("enter 1 for recharge");

    int option = Integer.parseInt(bufferedReader.readLine());
    if (option == 0) {
        try {
            connection = UserConnection.getConnection();
            preparedStatement = connection.prepareStatement("select balance,validity_in_mounth from userdetails where user_mobilenumber=?");
            try {
                preparedStatement.setLong(1, mobilenumber);
                preparedStatement.execute();
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select * from  userdetails");
                Formatter formatter = new Formatter();
                formatter.format("%15s %15s %15s %15s %15s %15s \n", "usermoblienumber", "username", "useremail", "simtype", "balance", "validity in months");
                while (resultSet.next()) {
                    formatter.format("%15s %15s %15s %15s %15s %15s \n", resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
                            resultSet.getString(4), resultSet.getInt(5), resultSet.getInt(6));
                }
                System.out.println(formatter);
            } catch (Exception exception) {
///               System.out.println("moblie number does not match");
                exception.printStackTrace();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
        else if (option == 1) {
            try {
                connection = UserConnection.getConnection();
                System.out.println("enter the recharge amount");
                int recharge = Integer.parseInt(bufferedReader.readLine());
                preparedStatement = connection.prepareStatement("update userdetails set balance=balance+? where user_mobilenumber=? ");
                preparedStatement.setInt(1, recharge);
                preparedStatement.setLong(2, mobilenumber);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement("update userdetails set validity_in_mounth =validity_in_mounth +1 where user_mobilenumber=? ");
                preparedStatement.setLong(1, mobilenumber);
                preparedStatement.executeUpdate();
                System.out.println("****************** RECHARGE IS DONE ******************");

            } catch(SQLException e){
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
    }
    }




    public void newConnection(){
        try {
            connection=UserConnection.getConnection();
            System.out.println("enter the name");
            String username=bufferedReader.readLine();
            System.out.println("enter the email");
            String useremail=bufferedReader.readLine();
            System.out.println("enter the initial balance");
            int balance=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the simtype either prepaid or postpaid");
            String simtype=bufferedReader.readLine();
            Random random=new Random();
            long mobile=random.nextInt(899999999)+900000000;
            preparedStatement=connection.prepareStatement("insert into userdetails (user_mobilenumber,user_name,email, simtype ,balance,validity_in_mounth) " +
                    "values (?,?,?,?,?,?)");
            preparedStatement.setLong(1,mobile);
            preparedStatement.setString(2,username);
            preparedStatement.setString(4,simtype);
            preparedStatement.setString(3,useremail);
            preparedStatement.setInt(5,balance);
            preparedStatement.setInt(6,1);
            preparedStatement.execute();
            System.out.println("*********** NEW USER ADDED******************");
            System.out.println("*********** details******************");
            statement=connection.createStatement();
            resultSet=statement.executeQuery("select * from userdetails");
            Formatter formatter=new Formatter();
            formatter.format("%15s %15s %15s %15s %15s %15s \n","usermoblienumber","username","useremail","simtype","balance","validity in months");
            while (resultSet.next()){
                formatter.format("%15s %15s %15s %15s %15s %15s \n",resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),
                        resultSet.getString(4),resultSet.getInt(5),resultSet.getInt(6));
            }
            System.out.println(formatter);


        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public void showDetails()  {
        try {
            connection=UserConnection.getConnection();
            statement=connection.createStatement();
            resultSet=statement.executeQuery("select * from userdetails");
            Formatter formatter=new Formatter();
            formatter.format("%15s %15s %15s %15s %15s %15s \n","usermoblienumber","username","useremail","simtype","balance","validity in months");
            while (resultSet.next()){
                formatter.format("%15s %15s %15s %15s %15s %15s \n",resultSet.getLong(1),resultSet.getString(2),resultSet.getString(3),
                        resultSet.getString(4),resultSet.getInt(5),resultSet.getInt(6));
            }
            System.out.println(formatter);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
/*
            +--------------------+-------------+------+-----+---------+-------+
| Field              | Type        | Null | Key | Default | Extra |
+--------------------+-------------+------+-----+---------+-------+
| user_mobilenumber  | bigint      | NO   | PRI | NULL    |       |
| user_name          | varchar(55) | YES  |     | NULL    |       |
| email              | varchar(55) | YES  |     | NULL    |       |
| simtype            | varchar(55) | YES  |     | NULL    |       |
| balance            | int         | YES  |     | 0       |       |
| validity_in_mounth | int         | YES  |     | 0       |       |
+--------------------+-------------+------+-----+---------+-------+

 */