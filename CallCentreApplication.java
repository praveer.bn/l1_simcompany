package simcompany;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CallCentreApplication {
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
boolean flag=true;
CallCentreOperation callCentreOperation=new CallCentreOperation();
public  void menu() throws IOException {
        while (flag) {
            System.out.println("0 for exit");
            System.out.println("1 for prepaid use");
            System.out.println("2 for new connection");//adding new user
            System.out.println("3 show all user");
            System.out.println("enter the option");
            int option=Integer.parseInt(bufferedReader.readLine());
            switch (option){
                case 0:
                    flag=false;
                    break;
                case 1:
                    callCentreOperation.prepaid();
                    break;
                case 2:
                    callCentreOperation.newConnection();
                    break;
                case 3:
                    callCentreOperation.showDetails();
                    break;
                default:
                    System.out.println("WRONG INPUT");
            }


        }
    }

    public static void main(String[] args) throws IOException {
        new CallCentreApplication().menu();
    }
}
